const { Teacher, Student, Sequelize, sequelize } = require('../models');
const validator = require('validator');
const { to, TE } = require('../services/util.service');
const studentService = require('../services/student.service');
const ERRORS = require("../const/errors");

const commonStudents = async function (teachersData) {
    if (!teachersData || teachersData.length == 0)
        return [];
    let err, students;
    [err, students] = await to(sequelize.query("select s.name from Students s join StudentTeacher st on s.id = st.StudentId join Teachers t on t.id = st.TeacherId where t.name in (:list) group by s.id having count(st.TeacherId) = :length", { replacements: { list: teachersData, length: teachersData.length }, type: sequelize.QueryTypes.SELECT }));
    if (err)
        throw err;
    return students.map(function (student) { return student.name });
}
module.exports.commonStudents = commonStudents;

const suspend = async function (studentData) {
    let studentInfo = {
        name: studentData
    };
    [err, student] = await to(Student.findOne({
        where: studentInfo
    }));
    if (!student)
        throw ERRORS.STUDENT_NOT_EXIST;
    await to(student.destroy());
}
module.exports.suspend = suspend;
