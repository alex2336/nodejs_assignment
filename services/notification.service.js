const { Teacher, Student, Sequelize, sequelize } = require('../models');
const validator = require('validator');
const { to, TE, validateEmail } = require('../services/util.service');
const studentService = require('../services/student.service');
const ERRORS = require("../const/errors");

const extractStudentNames = function (notificationData) {
    let splitted = notificationData.split(" ");
    let studentNames = [];
    splitted.forEach(function (str) {
        if (str && str[0] == "@") {
            let remaining = str.substring(1);
            if (validateEmail(remaining))
                studentNames.push(remaining);
        }
    });
    return studentNames;
}

const retrieveForNotifications = async function (teacherData, notificationData) {
    let teacher, err;
    if (!validateEmail(teacherData))
        throw ERRORS.INVALID_EMAIL;
    let teacherInfo = {
        name: teacherData
    };
    [err, teacher] = await to(Teacher.findOne({
        where: teacherInfo
    }));
    if (!teacher) {
        throw ERRORS.TEACHER_NOT_EXIST;
    }
    [err, students] = await to(teacher.getStudents());
    let studentNames = extractStudentNames(notificationData);
    if (studentNames.length > 0) {
        //combine with @ students
        let studentsFromAt;
        [err, studentsFromAt] = await to(sequelize.query("select name from Students where name in (:list)", { replacements: { list: studentNames }, type: sequelize.QueryTypes.SELECT }));
        students = students.concat(studentsFromAt);
    }
    if (err)
        throw err;
    //get names only
    students = students.map(function (student) { return student.name });
    //remove duplicates
    return [...new Set(students)];
}
module.exports.retrieveForNotifications = retrieveForNotifications;