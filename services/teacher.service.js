const { Teacher, Student, Sequelize, sequelize } = require('../models');
const { to, TE, validateEmail } = require('./util.service');
const studentService = require('./student.service');
const ERRORS = require("../const/errors");

const register = async function (teacherData, studentsData) {
    let teacher, err;
    if (!validateEmail(teacherData))
        throw ERRORS.INVALID_EMAIL;
    let teacherInfo = {
        name: teacherData
    };
    [err, teacher] = await to(Teacher.findOne({
        where: teacherInfo
    }));
    if (!teacher) {
        [err, teacher] = await to(Teacher.create(teacherInfo));
    }
    await Promise.all(studentsData.map(async function (studentData) {
        if (!validateEmail(studentData))
            throw ERRORS.INVALID_EMAIL;
        let studentInfo = {
            name: studentData
        };
        [err, student] = await to(Student.findOne({
            where: studentInfo
        }));
        if (!student)
            [err, student] = await to(Student.create(studentInfo));
        await to(teacher.addStudent(student));
    }));
    if (err)
        throw err;
    return teacher;
}
module.exports.register = register;


