const { to, ReE, ReS } = require('../services/util.service');
const teacherService = require('../services/teacher.service');
const studentService = require('../services/student.service');
const notificationService = require('../services/notification.service');

const register = async function (req, res) {
    const body = req.body;
    let teacherData, studentsData, err, result;
    teacherData = body.teacher;
    studentsData = body.students;
    [err, result] = await to(teacherService.register(teacherData, studentsData));
    if (err) return ReE(res, err, 422);
    return ReS(res, { message: 'Successfully created new user.' }, 204);
}
module.exports.register = register;

const commonStudents = async function (req, res) {
    const query = req.query;
    let teachersData = query.teacher;
    //if only one teacher name, convert it to an array as well
    if (typeof teachersData == 'string')
        teachersData = [teachersData];
    //remove duplicates
    teachersData = [...new Set(teachersData)];
    let students;
    [err, students] = await to(studentService.commonStudents(teachersData));
    if (err) return ReE(res, err, 422);
    return ReS(res, { students: students }, 200);
}
module.exports.commonStudents = commonStudents;

const suspend = async function (req, res) {
    const body = req.body;
    let studentData;
    studentData = body.student;
    [err, result] = await to(studentService.suspend(studentData));
    if (err) return ReE(res, err, 422);
    return ReS(res, { message: 'Successfully suspended.' }, 204);
}
module.exports.suspend = suspend;

const retrieveForNotifications = async function (req, res) {
    const body = req.body;
    let teacherData = body.teacher;
    let notificationData = body.notification;
    [err, stduents] = await to(notificationService.retrieveForNotifications(teacherData, notificationData));
    if (err) return ReE(res, err, 422);
    return ReS(res, { stduents: stduents }, 200);
}
module.exports.retrieveForNotifications = retrieveForNotifications;