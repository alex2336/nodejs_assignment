let ERRORS = {};
ERRORS.INVALID_EMAIL = new Error("Email address is invalid.");
ERRORS.STUDENT_NOT_EXIST = new Error("Student does not exist.");
ERRORS.TEACHER_NOT_EXIST = new Error("Teacher does not exist");
module.exports = ERRORS;