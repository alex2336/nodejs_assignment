const express = require('express');
const router = express.Router();
const HomeController = require('../controllers/home.controller');

router.get('/', function (req, res, next) {
  res.json({ status: "success", message: "Ufinity API assignement, Alex Wu" })
});


router.post('/register', HomeController.register);
router.get('/commonStudents', HomeController.commonStudents);
router.post('/suspend', HomeController.suspend);
router.post('/retrievefornotifications', HomeController.retrieveForNotifications);

module.exports = router;
