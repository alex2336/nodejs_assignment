'use strict';
const { TE, to } = require('../services/util.service');
const CONFIG = require('../config/config');

module.exports = (sequelize, DataTypes) => {
    var Model = sequelize.define('Student', {
        name: { type: DataTypes.STRING, allowNull: false, validate: { isEmail: true } }
    });

    Model.associate = function (models) {
        this.Teachers = this.belongsToMany(models.Teacher, { through: 'StudentTeacher' });
    };

    Model.prototype.toWeb = function (pw) {
        return this.name;
    };

    return Model;
};
