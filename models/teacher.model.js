const { TE, to } = require('../services/util.service');

module.exports = (sequelize, DataTypes) => {
  var Model = sequelize.define('Teacher', {
    name: { type: DataTypes.STRING, allowNull: false, validate: { isEmail: true } }
  });

  Model.associate = function (models) {
    this.Students = this.belongsToMany(models.Student, { through: 'StudentTeacher' });
  };

  Model.prototype.toWeb = function (pw) {
    return this.name;
  };

  return Model;
};