const ERRORS = require("../const/errors");
const rewiremock = require('rewiremock').default;

const teacherSpy = jasmine.createSpyObj("Teacher", ["findOne", "create"]);
const studentSpy = jasmine.createSpyObj("Student", ["findOne", "create"]);
const validateEmailSpy = jasmine.createSpy("validateEmail");

const utilSpy = jasmine.createSpyObj("Teacher", ["validateEmail"]);

const teacherService = rewiremock.proxy(() => require('../services/teacher.service'), {
  '../models': {
    Teacher: teacherSpy,
    Student: studentSpy
  },
  './util.service': rewiremock('./util.service')
    .callThrough()
    .with({
      validateEmail: validateEmailSpy
    })
});
describe("Teacher Service", function () {

  describe("Register", function () {
    beforeEach(function () {
      rewiremock.enable();
      //default validate email return true
      validateEmailSpy.and.returnValue(true);
      //default find return null
      teacherSpy.findOne.and.returnValue(new Promise((res, rej) => { res(null) }));
      teacherSpy.create.and.returnValue(new Promise((res, rej) => { res(null) }));
      studentSpy.findOne.and.returnValue(new Promise((res, rej) => { res(null) }));
      studentSpy.create.and.returnValue(new Promise((res, rej) => { res(null) }));
    });

    afterEach(() => rewiremock.disable());

    it("should throw error if teacher name is not an email", async function () {
      validateEmailSpy.and.returnValue(false);
      let error;
      try {
        await teacherService.register("abc", []);
      } catch (e) {
        error = e;
      }
      expect(error).toEqual(ERRORS.INVALID_EMAIL);
    });

    it("should call Teacher.findOne to check if teacher exists", async function () {
      teacherSpy.create.calls.reset();
      await teacherService.register("a@t.com", []);
      expect(teacherSpy.findOne).toHaveBeenCalled();
      expect(teacherSpy.findOne).toHaveBeenCalledWith({ where: { name: "a@t.com" } });
    });

    it("if teacher does not exist, should create new teacher", async function () {
      teacherSpy.create.calls.reset();
      await teacherService.register("a@t.com", []);
      expect(teacherSpy.create).toHaveBeenCalled();
      expect(teacherSpy.create).toHaveBeenCalledWith({ name: "a@t.com" });
    });

    it("if teacher exists, should not create new teacher", async function () {
      teacherSpy.create.calls.reset();
      teacherSpy.findOne.and.returnValue(new Promise((res, rej) => { res({}) }));
      await teacherService.register("a@t.com", []);
      expect(teacherSpy.create).not.toHaveBeenCalled();
    });


    it("should call Student.findOne to check if student exists", async function () {
      teacherSpy.create.and.returnValue(new Promise((res, rej) => { res({ addStudent: () => { return new Promise((res, rej) => { res({}) }) } }) }));
      let error;
      await teacherService.register("a@t.com", ["a@s.com"]);
      expect(studentSpy.findOne).toHaveBeenCalled();
      expect(studentSpy.findOne).toHaveBeenCalledWith({ where: { name: "a@s.com" } });
    });

    it("if student does not exist, should create new student", async function () {
      teacherSpy.create.and.returnValue(new Promise((res, rej) => { res({ addStudent: () => { return new Promise((res, rej) => { res({}) }) } }) }));
      studentSpy.create.calls.reset();
      await teacherService.register("a@t.com", ["a@s.com"]);
      expect(studentSpy.create).toHaveBeenCalled();
      expect(studentSpy.create).toHaveBeenCalledWith({ name: "a@s.com" });
    });

    it("if student exists, should not create new student", async function () {
      teacherSpy.create.and.returnValue(new Promise((res, rej) => { res({ addStudent: () => { return new Promise((res, rej) => { res({}) }) } }) }));
      studentSpy.create.calls.reset();
      studentSpy.findOne.and.returnValue(new Promise((res, rej) => { res({}) }));
      await teacherService.register("a@t.com", ["a@s.com"]);
      expect(studentSpy.create).not.toHaveBeenCalled();
    });

    it("if student already exist, should add it to the teacher", async function () {
      let teacherObjSpy = jasmine.createSpyObj("teacher", ['addStudent'])
      teacherSpy.create.and.returnValue(new Promise((res, rej) => { res(teacherObjSpy) }));
      teacherObjSpy.addStudent.and.returnValue(new Promise((res, rej) => { res({}) }));
      studentSpy.create.calls.reset();
      studentSpy.findOne.and.returnValue(new Promise((res, rej) => { res({ name: "existing student" }) }));
      await teacherService.register("a@t.com", ["a@s.com"]);
      expect(teacherObjSpy.addStudent).toHaveBeenCalled();
      expect(teacherObjSpy.addStudent).toHaveBeenCalledWith({ name: "existing student" });
    });

    it("if a new student has been created,should add it to the teacher", async function () {
      let teacherObjSpy = jasmine.createSpyObj("teacher", ['addStudent'])
      teacherSpy.create.and.returnValue(new Promise((res, rej) => { res(teacherObjSpy) }));
      teacherObjSpy.addStudent.and.returnValue(new Promise((res, rej) => { res({}) }));
      studentSpy.create.calls.reset();
      studentSpy.findOne.and.returnValue(new Promise((res, rej) => { res(null) }));
      studentSpy.create.and.returnValue(new Promise((res, rej) => { res({ name: "new student" }) }));
      await teacherService.register("a@t.com", ["a@s.com"]);
      expect(teacherObjSpy.addStudent).toHaveBeenCalled();
      expect(teacherObjSpy.addStudent).toHaveBeenCalledWith({ name: "new student" });
    });
  });

});
